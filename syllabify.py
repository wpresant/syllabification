import math

# file object for dictionary
dictionary_file = open("mhyph.txt", "r", encoding="utf-8")
# file object for input
input_file = open("input.txt", "r")
# file object for syllabified output
output_file = open("output.txt", "w")

# get all data from dictionary
dictionary_text = dictionary_file.read()
# each word entry is now its own index
dictionary = dictionary_text.splitlines()

# structure containing all non-dictionary words and count
nonwords = {}

# list of line numbers with non-dictionary words
nonword_lines = []

# split input into lines
input_text = input_file.read()
input_lines = input_text.splitlines()

# count lines from 0
current_line = 0

# function for searching dictionary
# returns false if not in dictionary
# returns syllabified version of word if in dictionary
def syllabified(word):
    for entry in dictionary:
        # var to store each word in dictionary w/o syllable marker(s)
        plain_entry = []

        for i in range(0, len(entry)):
            if entry[i] != "¥":
                plain_entry.append(entry[i])

        plain_entry = "".join(plain_entry)

        # see if this dictionary word matches word from input
        # if it does, return syllabified version of word
        if plain_entry == word:
            # var to store adjusted/syllabified version
            final_word = []
            # copy dictionary entry, using | instead of yen symbol
            for i in range(0, len(entry)):
                if entry[i] == "¥":
                    final_word.append("|")
                else:
                    final_word.append(entry[i])
            final_word = "".join(final_word)
            return final_word
    # if whole dictionary searched without returning, return false
    return False
        

# scroll through all lines in text
for line in input_lines:
    # scroll through all words in line
    line_words = line.split()
    for word in line_words:
        # check to see if word is in dictionary (use boolean flags?)
        # how to search dictionary for given entry
        # steps to search dictionary go here
        result = syllabified(word)
            # examine result
            # if word is <= 2 letters long, don't bother
        if len(word) <= 2:
            line_words[line_words.index(word)] = word
        # if it's a syllabified word, put it in place of original word
        elif result != False:
            line_words[line_words.index(word)] = result
        # else if result was not in dictionary
        elif result == False:
            # first, try again removing "s" at the end
            if word[len(word) - 1] == "s":
                alt_word = word[0:len(word)-1]
                alt_result = syllabified(alt_word)
                # if new word WAS found in dict, add syllab version with the s
                if alt_result != False:
                    alt_result = alt_result.split()
                    alt_result.append("s")
                    alt_result = "".join(alt_result)
                    line_words[line_words.index(word)] = alt_result
            # next, try again removing "ing" from the end
            elif word[len(word) - 3:len(word)] == "ing":
                alt_word = word[0:len(word)-3]
                alt_result = syllabified(alt_word)
                # if new word WAS found in dict, add syllab version with -ing
                if alt_result != False:
                    alt_result = alt_result.split()
                    alt_result.append("|ing")
                    alt_result = "".join(alt_result)
                    line_words[line_words.index(word)] = alt_result
            # next, try again removing "ed" from the end
            elif word[len(word) - 2:len(word)] == "ed":
                alt_word = word[0:len(word)-2]
                alt_result = syllabified(alt_word)
                # if new word WAS found in dict, add syllab version with -ed
                if alt_result != False:
                    alt_result = alt_result.split()
                    alt_result.append("ed")
                    alt_result = "".join(alt_result)
                    line_words[line_words.index(word)] = alt_result
            # default - add word to nonwords list
            # if not already in nonwords list, add to list with count 1
            else:
                if word not in nonwords:
                    nonwords[word] = 1
                # if already in nonwords list, increment word's count
                else:
                    nonwords[word] += 1
                # if this line not in nonword_lines list, add line no. to list
                if current_line not in nonword_lines:
                    nonword_lines.append(current_line)
    # create a string for the modified line and write to file (w/ line #)
    # words separated by spaces
    new_text = " ".join(line_words)
    output_file.write(str(current_line) + "  " + str(new_text) + "\n")
    # increment line number
    current_line += 1

# final task: write all line numbers with non-words
output_file.write("\n" + "Lines with nonwords (counting from 0):" + "\n")
for number in nonword_lines:
    output_file.write(str(number) + "\n")

# final task: write all non-words and number of occurrences
output_file.write("\n" + "Nonword frequency:" + "\n")
for key in nonwords:
    output_file.write(str(key) + ": " + str(nonwords[key]) + "\n")

input_file.close()
output_file.close()
